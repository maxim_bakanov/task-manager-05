# Project Info

Task manager (shcool job #05)

# Developer Info

**NAME:** Maxim Bakanov

**E-MAIL:** dragonnirvald@gmail.com

# Software 

- JDK 1.8
- Apache Maven 3.6.3
- MS Windows 10

# Hardware 

**CPU:** Intel i7-4770 or highter

**RAM:** 16 Gb DDR3-2400MHz or highter

**ROM:** 500 Gb M2.NVMe SSD or highter

# Program build

```mvn clean packega```

# Program run

`java -jar ./task-manager.jar`

### Another files

[screenshots](https://drive.google.com/drive/folders/1sLliMcpP1QqHeUNpX11bE3LILbO0T4eH?usp=sharing)

