package ru.mbakanov.tm.constant;

public interface TerminalConst {
    String HELP = "help";
    String ABOUT = "about";
    String VERSION = "version";
    String VERSION_NUM = "1.0.0";
    String DEV_NAME = "Name: Maxim Bakanov";
    String DEV_EMAIL = "E-Mail: graonnirvald@gmail.com";
}
